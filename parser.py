import os
import sys
from jinja2 import Template, Environment, meta

import configparser

config = configparser.ConfigParser()
config.read('.ini')

api_docs = sys.stdin.read()

env = Environment()
ast = env.parse(api_docs)

vars = {}

for var in meta.find_undeclared_variables(ast):
    if var in config['PRODUCTION']:
        vars[var] = config['PRODUCTION'][var]
    else:
        vars[var] = ""

template = Template(api_docs)

rendered = template.render(vars)

print(rendered)
